build:
	@echo ---- pulling images ---
	docker-compose pull
	@echo ---- build ----
	docker-compose build
	@echo ---- fix permissions for linux machine ----
	bash fix-permissions.sh

run:
	@echo ---- starting local dev  @80 ----
	docker-compose up -d
	@echo ---- gets .env file ----
	docker cp microblog:/var/www/html/.env src/front/.env

stop:
	@echo ---- stopping local dev @80 ----
	docker-compose down

logs:
	@echo ---- displaying logs. ctrl+c to quit ----
	docker-compose logs -f

clean:
	@echo ---- clean dev environment ----
	docker-compose down --rmi all --volumes
