FROM composer:latest as builder
# RUN composer global require hirak/prestissimo
COPY ./ /home/microblog
WORKDIR /home/microblog
RUN cp /home/microblog/.env.example /home/microblog/.env && \
    composer install --ignore-platform-reqs -vvv && \
    php artisan key:generate && \
    chown -R 1000:1000 /home/microblog/storage && \
    chown 1000:1000 -R /home/microblog/bootstrap/cache && \
    find /home/microblog -name "vendor" -prune -o -type d -exec chmod 755 -v {} \; && \
    find /home/microblog -name "vendor" -prune -o -type f -exec chmod 644 -v {} \;

FROM php:7.4.1-apache
USER root
COPY --from=builder /home/microblog /var/www/html
WORKDIR /var/www/html

RUN chown -R www-data:www-data /var/www/html \
    && a2enmod rewrite

# RUN cp /home/microblog/.env /var/www/html/.env && \
#    ln -sf /var/www/html/.env /home/microblog/.env


