# Docker-Laravel 2

## Requirements:
1.	Docker desktop application.
2.  docker-compose
3.	Make sure “xampp” is not running.

## Procedure:
1.	Go to the path **“C:\Windows\System32\drivers\etc”**.
2.	Open the *“hosts”* file (as administrator using Notepad or via Visual Studio Code).
3.	Add the line “127.0.0.1 microblog.local.com” at the bottom part on the “hosts” file then save.
4.	Clone repository https://gitlab.com/pzapanta/docker-laravel-2 to a location desired.
5.	Go to Docker desktop dashboard.
6.	Select *“Setting”* (gear shaped icon).
7.	Select *“Resources”* tab, then “FILE SHARING” tab.
8.	Add the folder path where you cloned the repository.
9.	Click the “Add and Restart” button.
10.	Go to the root folder.
    *	Open a terminal and run the commands *(in that sequence)*:
        ```bash
        cp src/front/.env.example src/front/.env
        docker-compose pull
        docker-compose build
        docker-compose up -d
        docker cp microblog:/var/www/html/.env src/front/.env
        ```
11.	Go to Laravel root folder (enter “src/front” folder).
12.	Open a terminal and run the command *“php artisan key:generate”*.
13.	Now open a browser, go to https://microblog.local.com/.
14.	The default Laravel page should be visible. You’re now ready to code.
