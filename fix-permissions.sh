#!/bin/bash

if [ ! -f "src/front/.env" ]
then
    cp src/front/.env.example src/front/.env
fi

chown -R 99:99 src/front/storage
chown -R 99:99 src/front/bootstrap/cache
find src -name "vendor" -prune -o -type d -exec chmod 755 -v {} \;
find src -name "vendor" -prune -o -type f -exec chmod 644 -v {} \;
